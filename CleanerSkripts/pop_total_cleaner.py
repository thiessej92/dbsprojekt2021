import csv
from collections import deque

with open('poptemp.csv', 'w', newline='') as csvfile:
	writer = csv.writer(csvfile)
	fieldnames = ['Entity', 'Year', 'Count']
	writer.writerow(fieldnames)
	with open('population_total.csv', newline='') as source:
		reader = csv.DictReader(source)
		land = []
		for row in reader:
			if row['Country Name'] not in land:
				land.append(row['Country Name'])
		land.sort()
		land = deque(land)
		while land != deque([]):
			x = land.popleft()
			with open('population_total.csv', newline='') as source:
				reader = csv.DictReader(source)
				for row in reader:
					if row['Country Name'] == x:
						rowwrite = [row['Country Name'], row['Year'], row['Count']]
						writer.writerow(rowwrite)
					





with open('pop.csv', 'w', newline='') as csvfile:
	fieldnames = ['Entity']
	for i in range(1960, 2018):
		fieldnames.append(str(i))
	writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
	
	with open('poptemp.csv', newline='') as source:
		reader = csv.DictReader(source)

		data = {'Entity': ''}
		for row in reader:
			if row['Entity'] != data['Entity']:
				if data['Entity'] == '':
					writer.writeheader()
				else:
					writer.writerow(data)
				data = {'Entity': row['Entity']}
			data[row['Year']] = row['Count']