import csv


with open('gdp2.csv', 'w', newline='') as csvfile:
	writer = csv.writer(csvfile)
	
	with open('gdp.csv', newline='') as source:
		reader = csv.reader(source)
		for row in reader:
			row=row[:-1]
			writer.writerow(row)