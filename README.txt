Welcome to the Gitlab Repository for the Database-course-Project-submission of 2021

The Interaction with the Database aswell as the resulting interactive Visualization is
 Documented in the Jupyter Notebook, found in the root level folder of this Repro.


1. MYSQL download with:             sudo apt install mysql-server
2  MYSQL init 
    2.1 Start mysql with:           sudo service mysql start
    2.2 Run through mysql quided Setup

    2.3 run MYSQL monitor with:     sudo mysql
    2.4 setup user with:            CREATE USER mysqlUser@'localhost' IDENTIFIED BY 'pass12345'
    2.5 grant omni access with:     GRANT ALL PRIVILEGES ON * . * TO mysqlUser@'localhost';
    2.6                             FLUSH PRIVILEGES
    2.7 logout with:                exit;
    2.8 login with new User:        mysql -u mysqlUser -p

3 Database Setup
    3.1 clean CSV files             
    3.2 import csv files to db      
        3.2.1 files have to be in LOCAL file system under -> SHOW VARIABLES LIKE "secure_file_priv";
        aka. /var/lib/mysql-files/my.cnf
    
        3.2.2 allow local file upload SET GLOBAL local_infile=1
        3.2.3 login with flag --local_infile=1

        LOAD DATA LOCAL INFILE 'CLEAN DATA/<some csv>.csv' INTO TABLE <tablename> FIELDS TERMINATED BY ',' LINES TERMINATED BY '\n' IGNORE 1 ROWS;
        
    3.3 interface with db for vis  

SOURCES

https://towardsdatascience.com/building-an-interactive-python-dashboard-using-sql-and-datapane-46bd92294fd3

https://docs.python.org/3/library/csv.html

https://www.kaggle.com/sansuthi/life-expectancy

https://www.tutorialspoint.com/python3/python_database_access.html

https://dev.mysql.com/doc/refman/5.7/en/creating-database.html

https://datatofish.com/sql-to-pandas-dataframe/



