import mysql.connector
import csv
import os
import re

def importCsv(csvFilePath,filename,dbCursor,nonStringValueType = "int"):

    raw_csv = open(csvFilePath)                     # open csv File
    reader = csv.reader(raw_csv)                    # create CSV reader Object

    tablename = os.path.splitext(filename)[0]    # Isolate tablename from filepath

    tableShema = createSQLShema(reader.__next__(), nonStringValueType)

    dbCursor.execute("CREATE TABLE IF NOT EXISTS %s (%s)" % (tablename,tableShema))

    print("shema passed")

    #executeString = "LOAD DATA INFILE '%s' INTO TABLE %s FIELDS TERMINATED BY ',' LINES TERMINATED BY '\\n' IGNORE 1 ROWS;" % (csvFilePath,tablename)

    #dbCursor.execute(executeString)

''' 
    for row in reader:
        insertion = ""
        for val in row[:row.__len__()-1]:
            if val=="":
                insertion += "NULL,"
            else:
                insertion += "%s , " % val
        
        if row[row.__len__()-1] == "":
            insertion += "NULL"
        else:
            insertion += "%s" % row[row.__len__()-1]

        print("INSERT INTO %s VALUES (%s)" % (tablename,insertion))

        dbCursor.execute("INSERT INTO %s VALUES (%s)" % (tablename,insertion))

'''  # PLEASE KILL ME NOW WITH THIS BULLSHIT, IM USING SQL TO DO IT AND THATS FUCKING THAT




def hookSQLdb(user, password, database):     # mySQL localhost connector

    mydb = mysql.connector.connect(
        host="localhost",
        user= user,
        password= password,
        db= database,
        autocommit = True,
    )

    return mydb


def createSQLShema(csvLineList, nonStringValueType = "int"):

    formatstring = ""                                       # inititalze format String to be returned
    yearRegEx = re.compile('17\d\d|18\d\d|19\d\d|20\d\d')   # Regular Expression will catch all year Fields

    for value in csvLineList:
        m = yearRegEx.match (value)

        if value == csvLineList[csvLineList.__len__()-1]:   # catch for last input
            if m:
                formatstring += '`%s`' % (value) + " " + nonStringValueType
            else:
                formatstring += '%s' % (value) + " varchar(20)"

        else:                                               # if not last input instert comma for addage
            if m:
                formatstring += '`%s`' % (value) + " " + nonStringValueType + ","
            else:
                formatstring += '%s' % (value.replace(" ", "_")) + " varchar(20),"
            
    return formatstring



if __name__ == "__main__" :


    mydb = hookSQLdb("sqlu","mypassword","dbtest")
    mydbCursor = mydb.cursor()
    
    importCsv("/home/wslu/Documents/Python/DBS/CLEAN DATA/gdp.csv","gdp.csv",mydbCursor, "double")
    importCsv("CLEAN DATA/population_total.csv","population_total.csv",mydbCursor, "double")
    importCsv("CLEAN DATA/population_growth.csv","population_growth.csv",mydbCursor, "double")
    importCsv("CLEAN DATA/co2_emission.csv","co2_emission.csv",mydbCursor, "int")
    importCsv("CLEAN DATA/lifeExp.csv","lifeExp.csv",mydbCursor, "double")

