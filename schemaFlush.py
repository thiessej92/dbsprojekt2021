import mysql.connector
import csv
import os
import re

def shemaFlusher(csvFilePath,filename,dbCursor,nonStringValueType = "int"):
#funktion initialies empty table in mysqlDB to be manually filled

    raw_csv = open(csvFilePath)                     # open csv File
    reader = csv.reader(raw_csv)                    # create CSV reader Object

    tablename = os.path.splitext(filename)[0]    # Isolate tablename from filepath

    tableShema = createSQLShema(reader.__next__(), nonStringValueType)

    dbCursor.execute("CREATE TABLE IF NOT EXISTS %s (%s)" % (tablename,tableShema))

    print("shema passed")


def hookSQLdb(user, password, database):     # mySQL localhost connector

    mydb = mysql.connector.connect(
        host="localhost",
        user= user,
        password= password,
        db= database,
        autocommit = True,
    )

    return mydb


def createSQLShema(csvLineList, nonStringValueType = "int"):

    formatstring = ""                                       # inititalze format String to be returned
    yearRegEx = re.compile('17\d\d|18\d\d|19\d\d|20\d\d')   # Regular Expression will catch all year Fields

    for value in csvLineList:
        m = yearRegEx.match (value)

        if value == csvLineList[csvLineList.__len__()-1]:   # catch for last input
            if m:
                formatstring += '`%s`' % (value) + " " + nonStringValueType
            else:
                formatstring += '%s' % (value) + " varchar(20)"

        else:                                               # if not last input instert comma for addage
            if m:
                formatstring += '`%s`' % (value) + " " + nonStringValueType + ","
            else:
                formatstring += '%s' % (value.replace(" ", "_")) + " varchar(20),"
            
    return formatstring


if __name__ == "__main__" :

    mydb = hookSQLdb("sqlu","mypassword","dbtest")
    mydbCursor = mydb.cursor()
    
    shemaFlusher("/home/wslu/Documents/Python/DBS/CLEAN DATA/gdp.csv","gdp.csv",mydbCursor, "double")
    shemaFlusher("CLEAN DATA/population_total.csv","population_total.csv",mydbCursor, "double")
    shemaFlusher("CLEAN DATA/population_growth.csv","population_growth.csv",mydbCursor, "double")
    shemaFlusher("CLEAN DATA/co2_emission.csv","co2_emission.csv",mydbCursor, "int")
    shemaFlusher("CLEAN DATA/lifeExp.csv","lifeExp.csv",mydbCursor, "double")

