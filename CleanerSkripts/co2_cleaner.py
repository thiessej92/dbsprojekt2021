import csv

with open('co2_2.csv', 'w', newline='') as csvfile:
	fieldnames = ['Entity', 'Code']
	for i in range(1751, 2021):
		fieldnames.append(str(i))
	writer = csv.DictWriter(csvfile,fieldnames=fieldnames)
	
	with open('co2_emission.csv', newline='') as source:
		reader = csv.DictReader(source)

		data = {'Entity': ''}
		for row in reader:
			if row['Entity'] != data['Entity']:
				if data['Entity'] == '':
					writer.writeheader()
				else:
					writer.writerow(data)
				data = {'Entity': row['Entity'], 'Code': row['Code']}
			data[row['Year']] = row['Annual CO₂ emissions (tonnes )']