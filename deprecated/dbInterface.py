import mysql.connector
import csv
import os
import re

# csv in cleaned Form: Country, year1, ... , yearx with first row header
csvs = ["gdp.csv", "co2_emission.csv" ,"population_growth.csv", "population_total.csv", "lifeExp.csv"]

def addFromCSV(filenpath,dbcursor,tablename):
    rcsv = open(filenpath)
    reader = csv.reader(rcsv)
 
    csvhead = reader.__next__()
    formatstring = ""

    i = 0
    for x in csvhead:
        if x == csvhead[csvhead.__len__()-1]:
            formatstring += '%s' % (sqlFormatReturnedHead(x))
        else:
            formatstring += '%s,' % (sqlFormatReturnedHead(x))
        i += 1

    # 0 name, 1 shema
    sqlStatement = "CREATE TABLE IF NOT EXISTS %s (%s);" % (tablename, formatstring)

    # dbcursor.execute(sqlStatement.format(tablename))

    csvrow = reader.__next__()

    while (reader.__next__() != None):
        print(csvrow)
        csvrow = reader.__next__()
        
    print("done")



def sqlhook(sqlName ,sqlPassword, dbName):
# Function returns Mysql Database Connection

    mydb = mysql.connector.connect(
        host="localhost",
        user=sqlName,
        password=sqlPassword,
        db=dbName,
        autocommit = True,
    )

    return mydb

def sqlFormatReturnedHead (colName, nonStringValueType = "int"):

    yearreg = re.compile('17\d\d|18\d\d|19\d\d|20\d\d') # CHECK REGEX
    fit = yearreg.match(colName)

    if fit:  # if we recognized a year then the value is elemtal (int or double, i have to check)
        return '`%s`' % (colName) + " " + nonStringValueType
    else:
        return '%s varchar(20)' % (colName)


if __name__ == "__main__":

    mydb = sqlhook("mysqlUser","pass12345","tester")
    addFromCSV("DATA/lifeExp.csv", mydb.cursor(), "LifeExpectancy")

    # testdb = sqlhook("mysqlUser","pass12345","tester")
    #addFromCSV("DATA/"+csvs[4], testdb.cursor(),os.path.splitext(csvs[4])[0])

